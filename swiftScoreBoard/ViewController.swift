//
//  ViewController.swift
//  swiftScoreBoard
//
//  Created by Matthew Griffin on 7/4/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    let playerOneScore = UILabel ();
    let playerTwoScore = UILabel ();
    let playerOneKeeper = UIStepper ();
    let playerTwoKeeper = UIStepper ();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let doneButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton;
        doneButton.frame = CGRectMake(0, 0, 100, 30);
        doneButton.setTitle("Done", forState: UIControlState.Normal);
        doneButton.backgroundColor = UIColor.whiteColor();
        doneButton.addTarget(self, action: "doneButton:", forControlEvents: UIControlEvents.TouchUpInside);
        
        self.view.backgroundColor = UIColor.blackColor();
        let playeroneName = UITextField ();
        playeroneName.frame = CGRectMake(10, 30, self.view.frame.width/2 - 20, 20);
        playeroneName.backgroundColor = UIColor.whiteColor();
        playeroneName.inputAccessoryView = doneButton;
        let playerOne = UIView ();
        let playerTwo = UIView ();
        
        playerOne.backgroundColor = UIColor.purpleColor();
        playerTwo.backgroundColor = UIColor.orangeColor();
        playerOne.frame = CGRectMake(0, 0, self.view.frame.width/2, self.view.frame.height/4 * 3);
        playerTwo.frame = CGRectMake(self.view.frame.width/2, 0, self.view.frame.width/2, self.view.frame.height/4 * 3);
        self.view.addSubview(playerOne);
        self.view.addSubview(playerTwo);
        
        
        playerOneKeeper.frame = CGRectMake(self.view.frame.width/8, 60 + self.view.frame.width/2, 10, 10);
        playerTwoKeeper.frame = CGRectMake(self.view.frame.width/8 * 5, 60 + self.view.frame.width/2, 10, 10);
        playerOneKeeper.backgroundColor = UIColor.orangeColor();
        playerTwoKeeper.backgroundColor = UIColor.purpleColor();
        playerOneKeeper.tintColor = UIColor.purpleColor();
        playerTwoKeeper.tintColor = UIColor.orangeColor();
        playerOneKeeper.addTarget(self, action: "PlayerOneScore:", forControlEvents: UIControlEvents.ValueChanged);
        playerTwoKeeper.addTarget(self, action: "PlayerTwoScore:", forControlEvents: UIControlEvents.ValueChanged);
        
        playerOneKeeper.maximumValue = 21;
        playerTwoKeeper.maximumValue = 21;
        self.view.addSubview(playerOneKeeper);
        self.view.addSubview(playerTwoKeeper);
        
        
        let playertwoName = UITextField ();
        playertwoName.frame = CGRectMake(self.view.frame.width/2 + 10, 30, self.view.frame.width/2 - 20, 20);
        playertwoName.backgroundColor = UIColor.whiteColor();
        playertwoName.inputAccessoryView = doneButton;
        self.view.addSubview(playeroneName);
        self.view.addSubview(playertwoName);
        
        playerOneScore.frame = CGRectMake(10, 50, self.view.frame.width/2 - 20 , self.view.frame.width/2 - 20);
        
        playerOneScore.textAlignment = .Center;
        playerOneScore.font = UIFont(name: "systemFont", size: 20);
        playerOneScore.textColor = UIColor.whiteColor();
        playerOneScore.text = "0";
        
        
        playerTwoScore.frame = CGRectMake(30 + self.view.frame.width/2 - 20, 50, self.view.frame.width/2 - 20, self.view.frame.width/2 - 20);
        playerTwoScore.textAlignment = .Center;
        
        playerTwoScore.font = UIFont(name: "systemFont", size: 20);
        playerTwoScore.textColor = UIColor.whiteColor();
        playerTwoScore.text = "0";
        self.view.addSubview(playerTwoScore);
        self.view.addSubview(playerOneScore);
        
        let reseter = UIButton.buttonWithType(UIButtonType.System) as! UIButton;
        reseter.frame = CGRectMake(20, self.view.frame.height/4 * 3 + 20, self.view.frame.width - 40, 20);
        reseter.setTitle("Reset Scores", forState: UIControlState.Normal);
        reseter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal);
        reseter.backgroundColor = UIColor.greenColor();
        reseter.addTarget(self, action: "resetButton:", forControlEvents: UIControlEvents.TouchUpInside);
        self.view.addSubview(reseter);
        
    }
    
    func PlayerOneScore(sender: UIStepper) {
        playerOneScore.text = String(format: "%.0f", playerOneKeeper.value);
    }
    
    func PlayerTwoScore(sender: UIStepper)
    {
        playerTwoScore.text = String(format: "%.0f", playerTwoKeeper.value);
    }
    
    func resetButton(sender: UIButton!)
    {
        playerOneKeeper.value = 0;
        playerTwoKeeper.value = 0;
        playerOneScore.text = "0";
        playerTwoScore.text = "0";
    }
    
    func doneButton(sender: UIButton!)
    {
        self.view.endEditing(true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

